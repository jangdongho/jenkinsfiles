ID=$1
FULL_SECRET_NAME=cronous-kr/default
EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
region="`echo \"$EC2_AVAIL_ZONE\" | sed 's/[a-z]$//'`"
aws secretsmanager get-secret-value --region $region --secret-id=$FULL_SECRET_NAME --query SecretString --output text|jq -r .$ID

